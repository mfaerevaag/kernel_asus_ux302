#!/bin/sh
# Custom kernel build and install script for Asus UX302 (UX302LA/UX302LG)
# by motley.slate@gmail.com
#
# Custom settings:
#   Reduces DEFAULT_CONSOLE_LOGLEVEL from 7 to 4 (default for Arch and most other distros)
#   Reduces swappiness from 60 to 10 (quiets HDD on UX302LA when booting from SSD and using HDD for swap)
#
# Custom kernel configuration:
#   Removes configuration/building of many drivers and architecture choices in the kernel not 
#    applicable to this hardware or not commonly used.
#  Keeps noveau drivers for Nvidia card in UX302LG.
#  Keeps all options for popular devices that may be plugged in (mice, joysticks, other USB devices etc.)

# version info
base_version=3.16.1
local_version=-motley
download_source_base_url=https://www.kernel.org/pub/linux/kernel/v3.x/
download_file_base=linux-3.16.1
download_config_file_url=https://bitbucket.org/motley/kernel_asus_ux302/raw/68cb7e85f0a5be1644f43e1b1a8a616f972a4841/motley_asus_ux302_x86_64_defconfig

# Create a temp build directory for our work
tempbuild=~/motley_kernel_build_$base_version$local_version
echo "Downloading linux-mainline into a temp folder..."
mkdir $tempbuild
cd $tempbuild
# get the kernel source and place into the temp build directory
mainline_url=$download_source_base_url$download_file_base.tar.xz
wget -c $mainline_url
echo "Decompressing kernel source..."
tar -xvJf $download_file_base.tar.xz
cd $download_file_base

# Prep tree
echo "Prepping kernel tree for build..."
make mrproper

echo "Downloading custom kernel configuration for Asus UX302 (.config file)..."
# Download my customized config file
rm .config
wget $download_config_file_url --output-document=.config

# fetch Alcor Micro sd card driver patch and apply 
# Thanks Oleksij Rempel, source: http://www.spinics.net/lists/linux-mmc/msg25836.html
# Patch modified slightly for 3.15 due to variable rename in 3.15
wget https://bitbucket.org/motley/kernel_asus_ux302/raw/c3873e18a9161414d5009e3d23b49cccd70719be/mmc-PATCH-RFC-Alcor-Micro-AU6601.patch --output-document=mmc-PATCH-RFC-Alcor-Micro-AU6601.patch
patch -p 1 -i mmc-PATCH-RFC-Alcor-Micro-AU6601.patch

# update kernel version
sed -i "s/-mainline-motley/$local_version/" .config

# Reduce log level from 7 to 4
sed -i 's/define DEFAULT_CONSOLE_LOGLEVEL 7/define DEFAULT_CONSOLE_LOGLEVEL 4/' kernel/printk/printk.c

# Reduce swappiness level from 60 to 10
sed -i 's/int vm_swappiness = 60/int vm_swappiness = 10/' mm/vmscan.c

# Add -pipe to compilation flags to see if it builds faster
sed -i 's/-fno-delete-null-pointer-checks/-fno-delete-null-pointer-checks -pipe/' Makefile

# Build
echo "Building kernel...this will take some time..."
make -j6

# Install
echo "Removing old modules folder..."
rm -R /lib/modules/*motley*

echo "Installing modules and kernel..."
make modules_install
cp -v arch/x86/boot/bzImage /boot/vmlinuz-mainline-motley

echo "Create and install initramfs..."
mkinitcpio -k $base_version$local_version -c /etc/mkinitcpio.conf -g /boot/initramfs-mainline-motley.img

echo "Removing temp folder from home..."
cd ../..
rm -R $tempbuild

echo "Finished. Configure GRUB2/bootloader install to create a boot option for contents placed in /boot."
echo "Note: GRUB2/bootloader configuration only has to be done once since the same name is used."
